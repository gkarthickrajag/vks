const express = require('express');
const app = express();
const mongoose = require('./database/mongoose');
const dipic = require('./database/model/dipicModel');
const smdic = require('./database/model/smdicModel');
const dipcapacitor = require('./database/model/dipcapacitorsModel');
const smdcapacitor = require('./database/model/smdcapacitorModel');
const diptransistor = require('./database/model/diptransistorModel');
const smdtransistor = require('./database/model/smdtransistorModel');
const dipdiodes = require('./database/model/dipdiodesModel');
const smddiodes = require('./database/model/smddiodesModel');
const ctcoil = require('./database/model/ctcoilModel');
const dipresistance = require('./database/model/dipresistance');
const smdresistance = require('./database/model/smdresistance');
const salesitem = require('./database/model/salesItemsModel');
const boardinandout = require('./database/model/boardinandout')

app.use(express.json());

app.use((req,res,next)=>{
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})

//dipic
app.post('/dipic',(req, res) =>{
    (new dipic({'icnumber':req.body.icnumber,'qty':req.body.qty,'used':req.body.used,'instock':req.body.instock}))
    .save()
    .then((dipic) => res.send(dipic))
    .catch((error) => console.log("error"))
})

app.get('/dipic',(req,  res) =>{
    dipic.find({})
    .then(dipic => res.send(dipic))
    .catch((error) => console.log("error"))
})

app.get('/dipic/:dipicId', (req,res) =>{
    dipic.find({_id:req.params.dipicId})
    .then(dipic => res.send(dipic))
    .catch( (error) => console.log("error"))
})

app.patch('/dipic/:dipicId', (req,res) =>{
    dipic.findByIdAndUpdate({_id:req.params.dipicId},{$set:req.body})
    .then( dipic => res.send(dipic))
    .catch( (error) => console.log("error"))
})

app.delete('/dipic/:dipicId', (req,res) =>{
    dipic.findByIdAndDelete({_id:req.params.dipicId})
    .then( dipic => res.send(dipic))
    .catch((error)=>console.log("error"))
   // res.status(200).send(student);  
})

//smdic
app.post('/smdic',(req, res) =>{
    (new smdic({'icnumber':req.body.icnumber,'qty':req.body.qty,'used':req.body.used,'instock':req.body.instock}))
    .save()
    .then((smdic) => res.send(smdic))
    .catch((error) => console.log("error"))
})

app.get('/smdic',(req,  res) =>{
    smdic.find({})
    .then(smdic => res.send(smdic))
    .catch((error) => console.log("error"))
})

app.get('/smdic/:smdicId', (req,res) =>{
    smdic.find({_id:req.params.smdicId})
    .then(smdic => res.send(smdic))
    .catch( (error) => console.log("error"))
})

app.patch('/smdic/:smdicId', (req,res) =>{
    smdic.findByIdAndUpdate({_id:req.params.smdicId},{$set:req.body})
    .then( smdic => res.send(smdic))
    .catch( (error) => console.log("error"))
})

app.delete('/smdic/:smdicId', (req,res) =>{
    smdic.findByIdAndDelete({_id:req.params.smdicId})
    .then( smdic => res.send(smdic))
    .catch((error)=>console.log("error"))
   // res.status(200).send(student);  
})

//dipcapacitor
app.post('/dipcapacitor',(req, res) =>{
    (new dipcapacitor({'MFDvolts':req.body.MFDvolts,'qty':req.body.qty,'used':req.body.used,'instock':req.body.instock}))
    .save()
    .then((dipcapacitor) => res.send(dipcapacitor))
    .catch((error) => console.log("error"))
})

app.get('/dipcapacitor',(req,  res) =>{
    dipcapacitor.find({})
    .then(dipcapacitor => res.send(dipcapacitor))
    .catch((error) => console.log("error"))
})

app.get('/dipcapacitor/:dipcapacitorId', (req,res) =>{
    dipcapacitor.find({_id:req.params.dipcapacitorId})
    .then(dipcapacitor => res.send(dipcapacitor))
    .catch( (error) => console.log("error"))
})

app.patch('/dipcapacitor/:dipcapacitorId', (req,res) =>{
    dipcapacitor.findByIdAndUpdate({_id:req.params.dipcapacitorId},{$set:req.body})
    .then( dipcapacitor => res.send(dipcapacitor))
    .catch( (error) => console.log("error"))
})

app.delete('/dipcapacitor/:dipcapacitorId', (req,res) =>{
    dipcapacitor.findByIdAndDelete({_id:req.params.dipcapacitorId})
    .then( dipcapacitor => res.send(dipcapacitor))
    .catch((error)=>console.log("error"))
   // res.status(200).send(student);  
})

//smdcapacitor
app.post('/smdcapacitor',(req, res) =>{
    (new smdcapacitor({'values':req.body.values,'qty':req.body.qty,'used':req.body.used,'instock':req.body.instock}))
    .save()
    .then((smdcapacitor) => res.send(smdcapacitor))
    .catch((error) => console.log("error"))
})

app.get('/smdcapacitor',(req,  res) =>{
    smdcapacitor.find({})
    .then(smdcapacitor => res.send(smdcapacitor))
    .catch((error) => console.log("error"))
})

app.get('/smdcapacitor/:smdcapacitorId', (req,res) =>{
    smdcapacitor.find({_id:req.params.smdcapacitorId})
    .then(smdcapacitor => res.send(smdcapacitor))
    .catch( (error) => console.log("error"))
})

app.patch('/smdcapacitor/:smdcapacitorId', (req,res) =>{
    smdcapacitor.findByIdAndUpdate({_id:req.params.smdcapacitorId},{$set:req.body})
    .then( smdcapacitor => res.send(smdcapacitor))
    .catch( (error) => console.log("error"))
})

app.delete('/smdcapacitor/:smdcapacitorId', (req,res) =>{
    smdcapacitor.findByIdAndDelete({_id:req.params.smdcapacitorId})
    .then( smdcapacitor => res.send(smdcapacitor))
    .catch((error)=>console.log("error"))
   // res.status(200).send(student);  
})

//diptransistor
app.post('/diptransistor',(req, res) =>{
    (new diptransistor({'name':req.body.name,'qty':req.body.qty,'used':req.body.used,'instock':req.body.instock}))
    .save()
    .then((diptransistor) => res.send(diptransistor))
    .catch((error) => console.log("error"))
})

app.get('/diptransistor',(req,  res) =>{
    diptransistor.find({})
    .then(diptransistor => res.send(diptransistor))
    .catch((error) => console.log("error"))
})

app.get('/diptransistor/:diptransistorId', (req,res) =>{
    diptransistor.find({_id:req.params.diptransistorId})
    .then(diptransistor => res.send(diptransistor))
    .catch( (error) => console.log("error"))
})

app.patch('/diptransistor/:diptransistorId', (req,res) =>{
    diptransistor.findByIdAndUpdate({_id:req.params.diptransistorId},{$set:req.body})
    .then( diptransistor => res.send(diptransistor))
    .catch( (error) => console.log("error"))
})

app.delete('/diptransistor/:diptransistorId', (req,res) =>{
    diptransistor.findByIdAndDelete({_id:req.params.diptransistorId})
    .then( diptransistor => res.send(diptransistor))
    .catch((error)=>console.log("error"))
   // res.status(200).send(student);  
})

//smdtransistor
app.post('/smdtransistor',(req, res) =>{
    (new smdtransistor({'marking':req.body.marking,'values':req.body.values,'qty':req.body.qty,'used':req.body.used,'instock':req.body.instock}))
    .save()
    .then((smdtransistor) => res.send(smdtransistor))
    .catch((error) => console.log("error"))
})

app.get('/smdtransistor',(req,  res) =>{
    smdtransistor.find({})
    .then(smdtransistor => res.send(smdtransistor))
    .catch((error) => console.log("error"))
})

app.get('/smdtransistor/:smdtransistorId', (req,res) =>{
    smdtransistor.find({_id:req.params.smdtransistorId})
    .then(smdtransistor => res.send(smdtransistor))
    .catch( (error) => console.log("error"))
})

app.patch('/smdtransistor/:smdtransistorId', (req,res) =>{
    smdtransistor.findByIdAndUpdate({_id:req.params.smdtransistorId},{$set:req.body})
    .then( smdtransistor => res.send(smdtransistor))
    .catch( (error) => console.log("error"))
})

app.delete('/smdtransistor/:smdtransistorId', (req,res) =>{
    smdtransistor.findByIdAndDelete({_id:req.params.smdtransistorId})
    .then( smdtransistor => res.send(smdtransistor))
    .catch((error)=>console.log("error"))
   // res.status(200).send(student);  
})

//dipdiodes
app.post('/dipdiodes',(req, res) =>{
    (new dipdiodes({'values':req.body.values,'qty':req.body.qty,'used':req.body.used,'instock':req.body.instock}))
    .save()
    .then((dipdiodes) => res.send(dipdiodes))
    .catch((error) => console.log("error"))
})

app.get('/dipdiodes',(req,  res) =>{
    dipdiodes.find({})
    .then(dipdiodes => res.send(dipdiodes))
    .catch((error) => console.log("error"))
})

app.get('/dipdiodes/:dipdiodesId', (req,res) =>{
    dipdiodes.find({_id:req.params.dipdiodesId})
    .then(dipdiodes => res.send(dipdiodes))
    .catch( (error) => console.log("error"))
})

app.patch('/dipdiodes/:dipdiodesId', (req,res) =>{
    dipdiodes.findByIdAndUpdate({_id:req.params.dipdiodesId},{$set:req.body})
    .then( dipdiodes => res.send(dipdiodes))
    .catch( (error) => console.log("error"))
})

app.delete('/dipdiodes/:dipdiodesId', (req,res) =>{
    dipdiodes.findByIdAndDelete({_id:req.params.dipdiodesId})
    .then( dipdiodes => res.send(dipdiodes))
    .catch((error)=>console.log("error"))
   // res.status(200).send(student);  
})

//smddiodes
app.post('/smddiodes',(req, res) =>{
    (new smddiodes({'marking':req.body.marking,'values':req.body.values,'qty':req.body.qty,'used':req.body.used,'instock':req.body.instock}))
    .save()
    .then((smddiodes) => res.send(smddiodes))
    .catch((error) => console.log("error"))
})

app.get('/smddiodes',(req,  res) =>{
    smddiodes.find({})
    .then(smddiodes => res.send(smddiodes))
    .catch((error) => console.log("error"))
})

app.get('/smddiodes/:smddiodesId', (req,res) =>{
    smddiodes.find({_id:req.params.smddiodesId})
    .then(smddiodes => res.send(smddiodes))
    .catch( (error) => console.log("error"))
})

app.patch('/smddiodes/:smddiodesId', (req,res) =>{
    smddiodes.findByIdAndUpdate({_id:req.params.smddiodesId},{$set:req.body})
    .then( smddiodes => res.send(smddiodes))
    .catch( (error) => console.log("error"))
})

app.delete('/smddiodes/:smddiodesId', (req,res) =>{
    smddiodes.findByIdAndDelete({_id:req.params.smddiodesId})
    .then( smddiodes => res.send(smddiodes))
    .catch((error)=>console.log("error"))
   // res.status(200).send(student);  
})

//ctcoil
app.post('/ctcoil',(req, res) =>{
    (new ctcoil({'rationMM':req.body.rationMM,'qty':req.body.qty,'used':req.body.used,'instock':req.body.instock}))
    .save()
    .then((ctcoil) => res.send(ctcoil))
    .catch((error) => console.log("error"))
})

app.get('/ctcoil', (req,  res) =>{
    ctcoil.find({})
    .then(ctcoil => res.send(ctcoil))
    .catch((error) => console.log("error"))
})

app.get('/ctcoil/:ctcoilId', (req,res) =>{
    ctcoil.find({_id:req.params.ctcoilId})
    .then(ctcoil => res.send(ctcoil))
    .catch( (error) => console.log("error"))
})

app.patch('/ctcoil/:ctcoilId', (req,res) =>{
    ctcoil.findByIdAndUpdate({_id:req.params.ctcoilId},{$set:req.body})
    .then( ctcoil => res.send(ctcoil))
    .catch( (error) => console.log("error"))
})

app.delete('/ctcoil/:ctcoilId', (req,res) =>{
    ctcoil.findByIdAndDelete({_id:req.params.ctcoilId})
    .then( ctcoil => res.send(ctcoil))
    .catch((error)=>console.log("error"))
   // res.status(200).send(student);  
})

//dipresistance
app.post('/dipresistance',(req, res) =>{
    (new dipresistance({'watt':req.body.watt,'qty':req.body.qty,'used':req.body.used,'instock':req.body.instock}))
    .save()
    .then((dipresistance) => res.send(dipresistance))
    .catch((error) => console.log("error"))
})

app.get('/dipresistance', (req,  res) =>{
    dipresistance.find({})
    .then(dipresistance => res.send(dipresistance))
    .catch((error) => console.log("error"))
})

app.get('/dipresistance/:dipresistanceId', (req,res) =>{
    dipresistance.find({_id:req.params.dipresistanceId})
    .then(dipresistance => res.send(dipresistance))
    .catch( (error) => console.log("error"))
})

app.patch('/dipresistance/:dipresistanceId', (req,res) =>{
    dipresistance.findByIdAndUpdate({_id:req.params.dipresistanceId},{$set:req.body})
    .then( dipresistance => res.send(dipresistance))
    .catch( (error) => console.log("error"))
})

app.delete('/dipresistance/:dipresistanceId', (req,res) =>{
    dipresistance.findByIdAndDelete({_id:req.params.dipresistanceId})
    .then( dipresistance => res.send(dipresistance))
    .catch((error)=>console.log("error"))
   // res.status(200).send(student);  
})

//smdresistance
app.post('/smdresistance',(req, res) =>{
    (new smdresistance({'range':req.body.range,'qty':req.body.qty,'used':req.body.used,'instock':req.body.instock}))
    .save()
    .then((smdresistance) => res.send(smdresistance))
    .catch((error) => console.log("error"))
})

app.get('/smdresistance', (req,  res) =>{
    smdresistance.find({})
    .then(smdresistance => res.send(smdresistance))
    .catch((error) => console.log("error"))
})

app.get('/smdresistance/:smdresistanceId', (req,res) =>{
    smdresistance.find({_id:req.params.smdresistanceId})
    .then(smdresistance => res.send(smdresistance))
    .catch( (error) => console.log("error"))
})

app.patch('/smdresistance/:smdresistanceId', (req,res) =>{
    smdresistance.findByIdAndUpdate({_id:req.params.smdresistanceId},{$set:req.body})
    .then( smdresistance => res.send(smdresistance))
    .catch( (error) => console.log("error"))
})

app.delete('/smdresistance/:smdresistanceId', (req,res) =>{
    smdresistance.findByIdAndDelete({_id:req.params.smdresistanceId})
    .then( smdresistance => res.send(smdresistance))
    .catch((error)=>console.log("error"))
   // res.status(200).send(student);  
})

//salesitem
app.post('/salesitems',(req, res) =>{
    (new salesitem({'materialdescription':req.body.materialdescription,'qty':req.body.qty,'used':req.body.used,'instock':req.body.instock}))
    .save()
    .then((salesitem) => res.send(salesitem))
    .catch((error) => console.log("error"))
})

app.get('/salesitems', (req,  res) =>{
    salesitem.find({})
    .then(salesitem => res.send(salesitem))
    .catch((error) => console.log("error"))
})

app.get('/salesitems/:salesitemsId', (req,res) =>{
    salesitem.find({_id:req.params.salesitemsId})
    .then(salesitem => res.send(salesitem))
    .catch( (error) => console.log("error"))
})

app.patch('/salesitems/:salesitemsId', (req,res) =>{
    salesitem.findByIdAndUpdate({_id:req.params.salesitemsId},{$set:req.body})
    .then( salesitem => res.send(salesitem))
    .catch( (error) => console.log("error"))
})

app.delete('/salesitems/:salesitemsId', (req,res) =>{
    salesitem.findByIdAndDelete({_id:req.params.salesitemsId})
    .then( salesitem => res.send(salesitem))
    .catch((error)=>console.log("error"))
   // res.status(200).send(student);  
})

//boardinandout
app.post('/boardinandout',(req, res) =>{
    (new boardinandout({'dc':req.body.dc,'companyname':req.body.companyname,'boardname1':req.body.boardname1,'boardname2':req.body.boardname2,'boardname3':req.body.boardname3,'boardname4':req.body.boardname4,'boardname5':req.body.boardname5,
    'sno1':req.body.sno1,'qty1':req.body.qty1,'sno2':req.body.sno2,'qty2':req.body.qty2,'sno3':req.body.sno3,'qty3':req.body.qty3,'sno4':req.body.sno4,'qty4':req.body.qty4,'sno5':req.body.sno5,'qty5':req.body.qty5,
    'problem1':req.body.problem1,'resolution1':req.body.resolution1,'handledperson1':req.body.handledperson1,
    'problem2':req.body.problem2,'resolution2':req.body.resolution2,'handledperson2':req.body.handledperson2,
    'problem3':req.body.problem3,'resolution3':req.body.resolution3,'handledperson3':req.body.handledperson3,
    'problem4':req.body.problem4,'resolution4':req.body.resolution4,'handledperson4':req.body.handledperson4,
    'problem5':req.body.problem5,'resolution5':req.body.resolution5,'handledperson5':req.body.handledperson5,
    'remarks':req.body.remarks,'inDate':req.body.inDate,'outDate':req.body.outDate}))
    .save()
    .then((boardinandout) => res.send(boardinandout))
    .catch((error) => console.log("error"))
    
})

app.get('/boardinandout', (req,  res) =>{
    boardinandout.find({})
    .then(boardinandout => res.send(boardinandout))
    .catch((error) => console.log("error"))
})

// app.get('/boardinandout/:date', (req,  res) =>{
//     boardinandout.find({"inDate": {$lt: req.params.date}})
//     .then(boardinandout => res.send(boardinandout))
//     .catch((error) => console.log("error"))
// })

app.get('/boardinandout/:boardinandoutId', (req,res) =>{
    boardinandout.find({_id:req.params.boardinandoutId})
    .then(boradinandout => res.send(boradinandout))
    .catch( (error) => console.log("error"))
})

app.patch('/boardinandout/:boardinandoutId', (req,res) =>{
    boardinandout.findByIdAndUpdate({_id:req.params.boardinandoutId},{$set:req.body})
    .then( boradinandout => res.send(boradinandout))
    .catch( (error) => console.log("error"))
})

app.delete('/boardinandout/:boardinandoutId', (req,res) =>{
    boardinandout.findByIdAndDelete({_id:req.params.boardinandoutId})
    .then( boradinandout => res.send(boradinandout))
    .catch((error)=>console.log("error"))
   // res.status(200).send(student);  
})

app.listen(3000, () => console.log("server is connected on port 3000"));