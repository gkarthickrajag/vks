const mongoose = require('mongoose');

const boardinandoutSchema = new mongoose.Schema({
    dc :{
        type:String,
    },
    companyname: {
        type: String,
    },
    boardname1: {
        type: String,
    },
    boardname2: {
        type: String,
    },
    boardname3: {
        type: String,
    },
    boardname4: {
        type: String,
    },
    boardname5: {
        type: String,
    },
    sno1:{
        type:String,
    },
    qty1:{
        type:String,
    },
    sno2:{
        type:String,
    },
    qty2:{
        type:String,
    },
    sno3:{
        type:String,
    },
    qty3:{
        type:String,
    },
    sno4:{
        type:String,
    },
    qty4:{
        type:String,
    },
    sno5:{
        type:String,
    },
    qty5:{
        type:String,
    },
    problem1: {
        type: String,
    },
    resolution1: {
        type: String,
    },
    handledperson1: {
        type: String,
    },
    problem2: {
        type: String,
    },
    resolution2: {
        type: String,
    },
    handledperson2: {
        type: String,
    },
    problem3: {
        type: String,
    },
    resolution3: {
        type: String,
    },
    handledperson3: {
        type: String,
    },
    problem4: {
        type: String,
    },
    resolution4: {
        type: String,
    },
    handledperson4: {
        type: String,
    },
    problem5: {
        type: String,
    },
    resolution5: {
        type: String,
    },
    handledperson5: {
        type: String,
    },
    remarks: {
        type: String,
    },
    inDate: {
        type: Date,
    },
    outDate: {
        type: Date,
    },
   

});

const boardinandout = mongoose.model('boardinandout', boardinandoutSchema);

module.exports = boardinandout;