const mongoose = require ('mongoose');

const ctcoilSchema = new mongoose.Schema({
    rationMM:{
        type: String,
        trim: true,
    },
    qty: {
        type: String,
        trim:true,
    },
    used:{
        type: String,
        trim:true,
    },
    instock :{
        type: String,
        trim:true,
    }
});

const ctcoil = mongoose.model('ctcoil',ctcoilSchema);

module.exports = ctcoil;