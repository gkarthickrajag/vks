const mongoose = require ('mongoose');

const DipCapacitorSchema = new mongoose.Schema({
    MFDvolts:{
        type: String,
        trim:true,
    },
    qty: {
        type: String,
        trim:true,
    },
    used:{
        type: String,
        trim:true,
    },
    instock :{
        type: String,
        trim:true,
    }
});

const DipCapacitor = mongoose.model('DipCapacitor',DipCapacitorSchema);

module.exports = DipCapacitor