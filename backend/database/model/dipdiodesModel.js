const mongoose = require ('mongoose');

const dipdiodesSchema = new mongoose.Schema({
     values :{
        type: String,
        trim:true,
    },
    qty: {
        type: String,
        trim:true,
    },
    used:{
        type: String,
        trim:true,
    },
    instock :{
        type: String,
        trim:true,
    }
});

const dipdiodes = mongoose.model('dipdiodes',dipdiodesSchema);

module.exports = dipdiodes;