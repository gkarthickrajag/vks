const mongoose = require('mongoose');

const dipresistanceSchema = new mongoose.Schema({
    watt: {
        type: String,
        trim: true,
    },
    qty: {
        type: String,
        trim: true,
    },
    used: {
        type: String,
        trim: true,
    },
    instock: {
        type: String,
        trim: true,
    }
});

const dipresistance = mongoose.model('dipresistance', dipresistanceSchema);

module.exports = dipresistance;