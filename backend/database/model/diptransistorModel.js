const mongoose = require ('mongoose');

const diptransistorSchema = new mongoose.Schema({
     name :{
        type: String,
        trim:true,
    },
    qty: {
        type: String,
        trim:true,
    },
    used:{
        type: String,
        trim:true,
    },
    instock :{
        type: String,
        trim:true,
    }
});

const diptransistor = mongoose.model('diptransistor',diptransistorSchema);

module.exports = diptransistor;