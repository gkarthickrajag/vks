const mongoose = require ('mongoose');

const salesitemsSchema = new mongoose.Schema({
        materialdescription:{
        type: String,
        trim: true,
            },
    qty: {
        type: String,
        trim:true,
    },
    used:{
        type: String,
        trim:true,
    },
    instock :{
        type: String,
        trim:true,
    }
});

const salesitems = mongoose.model('salesitems',salesitemsSchema);

module.exports = salesitems;