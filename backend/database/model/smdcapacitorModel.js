const mongoose = require ('mongoose');

const SMDCapacitorSchema = new mongoose.Schema({
     values :{
        type: String,
        trim:true,
    },
    qty: {
        type: String,
        trim:true,
    },
    used:{
        type: String,
        trim:true,
    },
    instock :{
        type: String,
        trim:true,
    }
});

const SMDCapacitor = mongoose.model('SMDCapacitor',SMDCapacitorSchema);

module.exports = SMDCapacitor;