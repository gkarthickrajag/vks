const mongoose = require ('mongoose');

const smddiodesSchema = new mongoose.Schema({
    marking:{
        type: String,
        trim: true,
    },
     values :{
        type: String,
        trim:true,
    },
    qty: {
        type: String,
        trim:true,
    },
    used:{
        type: String,
        trim:true,
    },
    instock :{
        type: String,
        trim:true,
    }
});

const smddiodes = mongoose.model('smddiodes',smddiodesSchema);

module.exports = smddiodes;