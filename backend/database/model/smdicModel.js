const mongoose = require ('mongoose');

const SMDICSchema = new mongoose.Schema({
    icnumber: {
        type: String,
        trim:true,
    },
    qty: {
        type: String,
        trim:true,
    },
    used:{
        type: String,
        trim:true,
    },
    instock :{
        type: String,
        trim:true,
    }
});

const SMDIC = mongoose.model('SMDIC',SMDICSchema);

module.exports = SMDIC