const mongoose = require ('mongoose');

const smdresistanceSchema = new mongoose.Schema({
        range:{
        type: String,
        trim: true,
            },
    qty: {
        type: String,
        trim:true,
    },
    used:{
        type: String,
        trim:true,
    },
    instock :{
        type: String,
        trim:true,
    }
});

const smdresistance = mongoose.model('smdresistance',smdresistanceSchema);

module.exports = smdresistance;