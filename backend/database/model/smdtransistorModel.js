const mongoose = require ('mongoose');

const smdtransistorSchema = new mongoose.Schema({
     marking :{
        type: String,
        trim:true,
    },
    values :{
        type: String,
        trim:true,
    },
    qty: {
        type: String,
        trim:true,
    },
    used:{
        type: String,
        trim:true,
    },
    instock :{
        type: String,
        trim:true,
    }
});

const smdtransistor = mongoose.model('smdtransistor', smdtransistorSchema);

module.exports = smdtransistor;