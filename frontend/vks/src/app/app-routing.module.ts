import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DipicComponent} from './dipic/dipic.component'
import { AppComponent } from './app.component';
import { SmdicComponent } from './smdic/smdic.component';
import { DipcapacitorComponent } from './dipcapacitor/dipcapacitor.component';
import { SmdcapacitorComponent } from './smdcapacitor/smdcapacitor.component';
import { DiptransistorComponent } from './diptransistor/diptransistor.component';
import { SmdtransistorComponent } from './smdtransistor/smdtransistor.component';
import { DipdiodesComponent } from './dipdiodes/dipdiodes.component';
import { SmddiodesComponent } from './smddiodes/smddiodes.component';
import { CtcoilComponent } from './ctcoil/ctcoil.component';
import { DipresistanceComponent } from './dipresistance/dipresistance.component';
import { SmdresistanceComponent } from './smdresistance/smdresistance.component';
import { SalesitemsComponent } from './salesitems/salesitems.component';
import { HomeComponent } from './home/home.component';
import { BoardinandoutComponent } from './boardinandout/boardinandout.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'dipic', component: DipicComponent },
  { path: 'smdic', component: SmdicComponent },
  { path: 'dipcapacitor', component: DipcapacitorComponent },
  { path: 'smdcapacitor', component: SmdcapacitorComponent },
  { path: 'diptransistor', component: DiptransistorComponent },
  { path: 'smdtransistor', component: SmdtransistorComponent },
  { path: 'dipdiodes', component: DipdiodesComponent },
  { path: 'smddiodes', component: SmddiodesComponent },
  { path: 'ctcoil', component: CtcoilComponent },
  { path: 'dipresistance', component: DipresistanceComponent },
  { path: 'smdresistance', component: SmdresistanceComponent },
  { path: 'salesitems', component: SalesitemsComponent },
  { path: 'boardinandout', component: BoardinandoutComponent },
  
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
 
}