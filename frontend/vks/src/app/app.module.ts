import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { DipicComponent } from './dipic/dipic.component';
import { SmdicComponent } from './smdic/smdic.component';
import { DipcapacitorComponent } from './dipcapacitor/dipcapacitor.component';
import { SmdcapacitorComponent } from './smdcapacitor/smdcapacitor.component';
import { DiptransistorComponent } from './diptransistor/diptransistor.component';
import { SmdtransistorComponent } from './smdtransistor/smdtransistor.component';
import { DipdiodesComponent } from './dipdiodes/dipdiodes.component';
import { SmddiodesComponent } from './smddiodes/smddiodes.component';
import { CtcoilComponent } from './ctcoil/ctcoil.component';
import { DipresistanceComponent } from './dipresistance/dipresistance.component';
import { SmdresistanceComponent } from './smdresistance/smdresistance.component';
import { SalesitemsComponent } from './salesitems/salesitems.component';
import { BoardinandoutComponent } from './boardinandout/boardinandout.component';
import {MaterialModule} from './material/material.module';
import { DateTimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { BoardinandoutService } from './shared/service/boardinandout.service';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';


@NgModule({
  declarations: [
    AppComponent,
    DipicComponent,
    SmdicComponent,
    DipcapacitorComponent,
    SmdcapacitorComponent,
    DiptransistorComponent,
    SmdtransistorComponent,
    DipdiodesComponent,
    SmddiodesComponent,
    CtcoilComponent,
    DipresistanceComponent,
    SmdresistanceComponent,
    SalesitemsComponent,
    BoardinandoutComponent,
    HomeComponent,
    LoginComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    DateTimePickerModule,
    MDBBootstrapModule,
    
  ],
  providers: [BoardinandoutService],
  bootstrap: [AppComponent]
})
export class AppModule { }
