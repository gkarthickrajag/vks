import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardinandoutComponent } from './boardinandout.component';

describe('BoardinandoutComponent', () => {
  let component: BoardinandoutComponent;
  let fixture: ComponentFixture<BoardinandoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardinandoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardinandoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
