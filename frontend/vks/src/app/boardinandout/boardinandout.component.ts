import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import {BoardinandoutService} from '../shared/service/boardinandout.service';
import {Boardinandout} from '../shared/model/boardinandoutModel'
import { MatTableDataSource } from '@angular/material/table';


declare var M: any;
interface HandledPersons {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-boardinandout',
  templateUrl: './boardinandout.component.html',
  styleUrls: ['./boardinandout.component.scss'],
  providers: [BoardinandoutService]
})
export class BoardinandoutComponent implements OnInit {
  displayedColumns: string[] = ['DC','CompanyName', 'BoardName', 'Problem', 'Resolution', 'Handled Person','Remarks','InDate','OutDate','Action'];
  dataSource: any;
  checked:Boolean;
  handledPersons:HandledPersons[]= [
    {value:'Udhaya Kumar-0', viewValue:'Udhayakumar'},
    {value:'Gowtham-1', viewValue:'Gowtham'},
    {value:'Kathir-2', viewValue:'Kathir'},
    {value:'Mani-3', viewValue:'Mani'},
    {value:'Karthik', viewValue:'Karthik'}
  ];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

  }
  constructor(public boardinandoutService: BoardinandoutService) { }

   
  ngOnInit() {
    this.resetForm();
    this.refreshboardinandoutList();
   
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.boardinandoutService.selectedBoardinandout = {
      _id: "",
      dc:"",
      companyname:"",
      boardname1:"",
      boardname2:"",
      boardname3:"",
      boardname4:"",
      boardname5:"",
      sno1:"",
      qty1:"",
      sno2:"",
      qty2:"",
      sno3:"",
      qty3:"",
      sno4:"",
      qty4:"",
      sno5:"",
      qty5:"",
      problem1:"",resolution1:"",handledperson1:"", problem2:"",resolution2:"",handledperson2:"", problem3:"",resolution3:"",handledperson3:"",
      problem4:"",resolution4:"",handledperson4:"", problem5:"",resolution5:"",handledperson5:"",
      remarks:"",
      inDate:null,
      outDate:null,
      
    }
  }

  
  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.boardinandoutService.postboardinandout(form.value).subscribe((res) => {
        console.log(form.value);
        this.resetForm(form);
        this.refreshboardinandoutList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    }
    else {
      this.boardinandoutService.putboardinandout(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshboardinandoutList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshboardinandoutList() {
    this.boardinandoutService.getboardinandoutList().subscribe((res) => {
      this.boardinandoutService.boardinandouts = res as Boardinandout[];
      this. dataSource = new MatTableDataSource(this.boardinandoutService.boardinandouts);
    });
  }

  onEdit(boardinandout: Boardinandout) {
    console.log(boardinandout)
    console.log("edit")
    this.boardinandoutService.selectedBoardinandout = boardinandout;
    
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.boardinandoutService.deleteboardinandout(_id).subscribe((res) => {
        this.refreshboardinandoutList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
