import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CtcoilComponent } from './ctcoil.component';

describe('CtcoilComponent', () => {
  let component: CtcoilComponent;
  let fixture: ComponentFixture<CtcoilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CtcoilComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CtcoilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
