import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {CtcoilService} from '../shared/service/ctcoil.service';
import {Ctcoil} from '../shared/model/ctcoilModel'
import { MatTableDataSource } from '@angular/material/table';


declare var M: any;

@Component({
  selector: 'app-ctcoil',
  templateUrl: './ctcoil.component.html',
  styleUrls: ['./ctcoil.component.scss'],
  providers: [CtcoilService]
})
export class CtcoilComponent implements OnInit {
  displayedColumns: string[] = ['Ration/MM', 'qty', 'Used', 'InStock','Action'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public ctcoilService: CtcoilService) { }

  ngOnInit() {
    this.resetForm();
    this.refreshctcoilList();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.ctcoilService.selectedCtcoil = {
      _id: "",
      rationMM: "",
      qty: "",
      used: "",
      instock: "",
    }
  }

  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.ctcoilService.postctcoil(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshctcoilList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    }
    else {
      this.ctcoilService.putctcoil(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshctcoilList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshctcoilList() {
    this.ctcoilService.getctcoilList().subscribe((res) => {
      this.ctcoilService.ctcoils = res as Ctcoil[];
      this.dataSource = new MatTableDataSource(this.ctcoilService.ctcoils);
    });
  }

  onEdit(ctcoil: Ctcoil) {
    console.log(ctcoil)
    this.ctcoilService.selectedCtcoil = ctcoil;
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.ctcoilService.deletectcoil(_id).subscribe((res) => {
        this.refreshctcoilList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
