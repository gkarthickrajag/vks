import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DipcapacitorComponent } from './dipcapacitor.component';

describe('DipcapacitorComponent', () => {
  let component: DipcapacitorComponent;
  let fixture: ComponentFixture<DipcapacitorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DipcapacitorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DipcapacitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
