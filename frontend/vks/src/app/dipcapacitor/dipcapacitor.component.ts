import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {DipcapacitorService} from '../shared/service/dipcapacitor.service';
import {Dipcapacitor} from '../shared/model/dipcapacitorModel'
import { MatTableDataSource } from '@angular/material/table';

declare var M: any;

@Component({
  selector: 'app-dipcapacitor',
  templateUrl: './dipcapacitor.component.html',
  styleUrls: ['./dipcapacitor.component.scss'],
  providers: [DipcapacitorService]
})
export class DipcapacitorComponent implements OnInit {
  displayedColumns: string[] = ['MFDvolts', 'qty', 'Used', 'InStock','Action'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public dipcapacitorService: DipcapacitorService) { }

  ngOnInit() {
    this.resetForm();
    this.refreshdipcapacitorList();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.dipcapacitorService.selectedDipcapacitor = {
      _id: "",
      MFDvolts: "",
      qty: "",
      used: "",
      instock: "",
    }
  }

  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.dipcapacitorService.postdipcapacitor(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshdipcapacitorList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    }
    else {
      this.dipcapacitorService.putdipcapacitor(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshdipcapacitorList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshdipcapacitorList() {
    this.dipcapacitorService.getdipcapacitorList().subscribe((res) => {
      this.dipcapacitorService.dipcapacitors = res as Dipcapacitor[];
      this.dataSource = new MatTableDataSource(this.dipcapacitorService.dipcapacitors);
    });
  }

  onEdit(dipcapacitor: Dipcapacitor) {
    this.dipcapacitorService.selectedDipcapacitor = dipcapacitor;
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.dipcapacitorService.deletedipcapacitor(_id).subscribe((res) => {
        this.refreshdipcapacitorList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
