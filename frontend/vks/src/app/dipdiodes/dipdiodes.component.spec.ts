import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DipdiodesComponent } from './dipdiodes.component';

describe('DipdiodesComponent', () => {
  let component: DipdiodesComponent;
  let fixture: ComponentFixture<DipdiodesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DipdiodesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DipdiodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
