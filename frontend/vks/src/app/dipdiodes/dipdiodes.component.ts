import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {DipdiodesService} from '../shared/service/dipdiodes.service';
import {Dipdiodes} from '../shared/model/dipdiodesModel'
import { MatTableDataSource } from '@angular/material/table';


declare var M: any;

@Component({
  selector: 'app-dipdiodes',
  templateUrl: './dipdiodes.component.html',
  styleUrls: ['./dipdiodes.component.scss'],
  providers: [DipdiodesService]
})
export class DipdiodesComponent implements OnInit {
  displayedColumns: string[] = ['Values', 'qty', 'Used', 'InStock','Action'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public dipdiodesService: DipdiodesService) { }

  ngOnInit() {
    this.resetForm();
    this.refreshdipdiodesList();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.dipdiodesService.selectedDipdiodes = {
      _id: "",
      values: "",
      qty: "",
      used: "",
      instock: "",
    }
  }

  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.dipdiodesService.postdipdiodes(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshdipdiodesList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    }
    else {
      this.dipdiodesService.putdipdiodes(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshdipdiodesList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshdipdiodesList() {
    this.dipdiodesService.getdipdiodesList().subscribe((res) => {
      this.dipdiodesService.dipdiodess = res as Dipdiodes[];
      this.dataSource = new MatTableDataSource(this.dipdiodesService.dipdiodess);
    });
  }

  onEdit(dipdiodes: Dipdiodes) {
    this.dipdiodesService.selectedDipdiodes = dipdiodes;
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.dipdiodesService.deletedipdiodes(_id).subscribe((res) => {
        this.refreshdipdiodesList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
