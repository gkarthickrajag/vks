import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DipicComponent } from './dipic.component';

describe('DipicComponent', () => {
  let component: DipicComponent;
  let fixture: ComponentFixture<DipicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DipicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DipicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
