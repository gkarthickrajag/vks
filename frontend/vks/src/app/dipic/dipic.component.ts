import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DipicService } from '../shared/service/dipic.service';
import { Dipic } from '../shared/model/dipicModel'
import { MdbTableDirective } from 'angular-bootstrap-md';
import { MatTableDataSource } from '@angular/material/table';


declare var M: any;

@Component({
  selector: 'app-dipic',
  templateUrl: './dipic.component.html',
  styleUrls: ['./dipic.component.scss'],
  providers: [DipicService]
})
export class DipicComponent implements OnInit {
  displayedColumns: string[] = ['IcNumber', 'qty', 'Used', 'InStock','Action'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  constructor(public dipicService: DipicService) { }  

  ngOnInit() {
    this.resetForm();
    this.refreshDipicList();
  
  }

  dipic = new Dipic();

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.dipicService.selectedDipic = {
      _id: "",
      icnumber: "",
      qty: "",
      used: "",
      instock: "",
    }
  }

  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.dipicService.postDipic(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshDipicList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    }
    else {
      this.dipicService.putDipic(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshDipicList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshDipicList() {
    this.dipicService.getDipicList().subscribe((res) => {
      this.dipicService.dipics = res as Dipic[];
      this.dataSource = new MatTableDataSource(this.dipicService.dipics);
    });
  }

  onEdit(dipic: Dipic) {
    console.log(dipic)
    this.dipicService.selectedDipic = dipic;
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.dipicService.deleteDipic(_id).subscribe((res) => {
        this.refreshDipicList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
