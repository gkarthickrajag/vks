import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DipresistanceComponent } from './dipresistance.component';

describe('DipresistanceComponent', () => {
  let component: DipresistanceComponent;
  let fixture: ComponentFixture<DipresistanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DipresistanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DipresistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
