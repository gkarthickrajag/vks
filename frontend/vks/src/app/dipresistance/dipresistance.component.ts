import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {DipresistanceService} from '../shared/service/dipresistance.service';
import {Dipresistance} from '../shared/model/dipresistanceModel'
import { MatTableDataSource } from '@angular/material/table';


declare var M: any;

@Component({
  selector: 'app-dipresistance',
  templateUrl: './dipresistance.component.html',
  styleUrls: ['./dipresistance.component.scss'],
  providers: [DipresistanceService]
})
export class DipresistanceComponent implements OnInit {
  displayedColumns: string[] = ['Quarter/Half/Two watts/load resistance', 'qty', 'Used', 'InStock','Action'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public dipresistanceService: DipresistanceService) { }

  ngOnInit() {
    this.resetForm();
    this.refreshdipresistanceList();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.dipresistanceService.selectedDipresistance = {
      _id: "",
      watt: "",
      qty: "",
      used: "",
      instock: "",
    }
  }

  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.dipresistanceService.postdipresistance(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshdipresistanceList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    }
    else {
      this.dipresistanceService.putdipresistance(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshdipresistanceList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshdipresistanceList() {
    this.dipresistanceService.getdipresistanceList().subscribe((res) => {
      this.dipresistanceService.dipresistances = res as Dipresistance[];
      this.dataSource = new MatTableDataSource(this.dipresistanceService.dipresistances);
    });
  }

  onEdit(dipresistance: Dipresistance) {
    this.dipresistanceService.selectedDipresistance = dipresistance;
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.dipresistanceService.deletedipresistance(_id).subscribe((res) => {
        this.refreshdipresistanceList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
