import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiptransistorComponent } from './diptransistor.component';

describe('DiptransistorComponent', () => {
  let component: DiptransistorComponent;
  let fixture: ComponentFixture<DiptransistorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiptransistorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiptransistorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
