import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {DiptransistorService} from '../shared/service/diptransistor.service';
import {Diptransistor} from '../shared/model/diptransistorModel'
import { MatTableDataSource } from '@angular/material/table';


declare var M: any;

@Component({
  selector: 'app-diptransistor',
  templateUrl: './diptransistor.component.html',
  styleUrls: ['./diptransistor.component.scss'],
  providers: [DiptransistorService]
})
export class DiptransistorComponent implements OnInit {
  displayedColumns: string[] = ['Name', 'qty', 'Used', 'InStock','Action'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public diptransistorService: DiptransistorService) { }

  ngOnInit() {
    this.resetForm();
    this.refreshdiptransistorList();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.diptransistorService.selectedDiptransistor = {
      _id: "",
      name: "",
      qty: "",
      used: "",
      instock: "",
    }
  }

  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.diptransistorService.postdiptransistor(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshdiptransistorList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    }
    else {
      this.diptransistorService.putdiptransistor(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshdiptransistorList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshdiptransistorList() {
    this.diptransistorService.getdiptransistorList().subscribe((res) => {
      this.diptransistorService.diptransistors = res as Diptransistor[];
      this.dataSource = new MatTableDataSource(this.diptransistorService.diptransistors);
    });
  }

  onEdit(diptransistor: Diptransistor) {
    this.diptransistorService.selectedDiptransistor = diptransistor;
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.diptransistorService.deletediptransistor(_id).subscribe((res) => {
        this.refreshdiptransistorList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
