import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import {BoardinandoutService} from '../shared/service/boardinandout.service';
import {Boardinandout} from '../shared/model/boardinandoutModel';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import {MdbTableDirective} from 'angular-bootstrap-md';
import { MatTableDataSource } from '@angular/material/table';
import { DatePipe } from '@angular/common';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

declare var M: any;
declare  var jQuery:  any;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  pipe = new DatePipe('en-US');
  

  displayedColumns: string[] = ['DC','CompanyName', 'BoardName', 'Handled Person','Remarks','InDate','OutDate','DC PDF'];
  // displayedColumns: string[] = ['CompanyName', 'BoardName', 'Problem', 'Resolution']

  title = 'vks';
  dataSource: any;
  checked:Boolean;
  i:any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

  }
 
  constructor(public boardinandoutService: BoardinandoutService) { }

  ngOnInit() {
    // this.resetForm();
    this.refreshboardinandoutList();
   
  
  }

  boardinandout = new Boardinandout();

  generatePdf(){
    const documentDefinition = this.getDocumentDefinition();
   
    pdfMake.createPdf(documentDefinition).open();
    pdfMake.createPdf(documentDefinition).download(this.boardinandoutService.selectedBoardinandout.outDate.toString().substr(0, 10)+'"DeliveryChalan".pdf');
   }

   getDocumentDefinition() {
  
  return{
    pageOrientation: 'landscape',
    content: [
      {
        text: 'GSTIN : 33AJJPV0727B1ZH                      Phone : 97863 00171,97863 00962',
        bold: true,
        fontSize: 10,
        alignment: 'left',
        margin: [0, 0, 0, 0],
      }, 
      {
        text: 'DELIVERY CHALAN',
        bold: true,
        fontSize: 15,
        alignment: 'left',
        margin: [110, 10, 0, 0]
      }, 
      {
        text: '_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _',
        bold: true,
        fontSize: 10,
        alignment: 'left',
        margin: [110, 0, 0, 0]
      }, 
      {
        text: 'VKS ELECTRONICS',
        bold: true,
        fontSize: 20,
        alignment: 'left',
        margin: [90, 0, 0, 0]
      }, 
      {
        text: 'Solution Provider for Industrial Electronics',
        fontSize: 10,
        alignment: 'left',
        margin: [80, 0, 0, 0]
      }, 
      {
        text: '5/32H, Lakshmi Garden, G.H. Back side,',
        fontSize: 10,
        alignment: 'left',
        margin: [85, 0, 0, 0]
      }, 
      {
        text: 'Masinaikenpatty, Ayothiyapattanam, Salem-636103,Tamil Nadu.',
        fontSize: 10,
        alignment: 'left',
        margin: [40, 0, 0, 0]
      }, 
      {
        text: 'Email : vkselectronicssalem@gmail.com.',
        fontSize: 10,
        alignment: 'left',
        margin: [75, 0, 0, 0]
      },
      {
        text: 'DC No:' + this.boardinandoutService.selectedBoardinandout.dc +'                                                                                         Date :' + this.pipe.transform(this.boardinandoutService.selectedBoardinandout.outDate, 'MM/dd/yyyy').toString(),
        fontSize: 10,
        alignment: 'left',
        margin: [0, 0, 0, 10]
      }, 
      {
        text: 'M/s:   ' +  this.boardinandoutService.selectedBoardinandout.companyname,
        fontSize: 15,
        alignment: 'left',
        margin: [0, 0, 0, 30]
      },
      {
        layout: 'headerLineOnly',
        table: {
          headerRows: 1,
          widths: [ '5%', '30%', '10%'],
          body: [
            [ 'S.No', 'Description', 'Quantity' ],
            [{ text: this.boardinandoutService.selectedBoardinandout.sno1, alignment: 'left',fontSize: 10 }, {text:this.boardinandoutService.selectedBoardinandout.boardname1,fontSize: 10}, {text:this.boardinandoutService.selectedBoardinandout.qty1,fontSize:10 }],
            [{ text:this.boardinandoutService.selectedBoardinandout.sno2,fontSize: 10}, {text:this.boardinandoutService.selectedBoardinandout.boardname2,fontSize: 10}, {text:this.boardinandoutService.selectedBoardinandout.qty2,fontSize:10 } ],
            [{ text:this.boardinandoutService.selectedBoardinandout.sno3,fontSize: 10},{text:this.boardinandoutService.selectedBoardinandout.boardname3,fontSize: 10}, {text:this.boardinandoutService.selectedBoardinandout.qty3,fontSize:10 }],
            [{ text:this.boardinandoutService.selectedBoardinandout.sno4,fontSize: 10}, {text:this.boardinandoutService.selectedBoardinandout.boardname4,fontSize: 10}, {text:this.boardinandoutService.selectedBoardinandout.qty4,fontSize:10 } ],
            [{ text:this.boardinandoutService.selectedBoardinandout.sno5,fontSize: 10}, {text:this.boardinandoutService.selectedBoardinandout.boardname5,fontSize: 10}, {text:this.boardinandoutService.selectedBoardinandout.qty5,fontSize:10 } ],
          ]
        }
      },
      {
        text: 'Customers Reference:',
        bold: true,
        fontSize: 10,
        alignment: 'left',
        margin: [0, 30, 0, 0]
      },  
      {
        text: ''+this.boardinandoutService.selectedBoardinandout.remarks,
        bold: true,
        fontSize: 10,
        alignment: 'left',
        margin: [20, 0, 0, 0]
      },  
      {
        text: 'Goods Received By                                                             For VKS ELECTRONICS',
        bold: true,
        fontSize: 10,
        alignment: 'left',
        margin: [0, 50, 0, 0]
      }, 
      
    ],
    }
  
  }
 

  refreshboardinandoutList() {
    this.boardinandoutService.getboardinandoutList().subscribe((res) => {
      this.boardinandoutService.boardinandouts = res as Boardinandout[];
      this. dataSource = new MatTableDataSource(this.boardinandoutService.boardinandouts);
     
    });
    
  }

  onEdit(boardinandout: Boardinandout) {
    console.log(boardinandout.outDate)
    this.boardinandoutService.selectedBoardinandout = boardinandout;
  }

  

}
