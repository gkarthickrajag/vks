import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Router} from '@angular/router';

declare var password: any;
declare var username: any;
declare var M: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: []
})
export class LoginComponent implements OnInit {
   username: any;
   password: any;
  constructor(public router:Router) { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {
    if((form.value.username == "VKS") && (form.value.password == "vks@123"))
    {
      console.log(form.value)
      console.log("success")
      this.router.navigate(['/home']);
    }
    else{
      M.toast({ html: 'Enter correct Username and Password', classes: 'rounded' });
    }
  }
}
