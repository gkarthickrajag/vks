import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {SalesitemsService} from '../shared/service/salesitems.service';
import {Salesitems} from '../shared/model/salesitemsModel'
import { MatTableDataSource } from '@angular/material/table';


declare var M: any;

@Component({
  selector: 'app-salesitems',
  templateUrl: './salesitems.component.html',
  styleUrls: ['./salesitems.component.scss'],
  providers: [SalesitemsService]
})
export class SalesitemsComponent implements OnInit {
  displayedColumns: string[] = ['Material Description', 'qty', 'Used', 'InStock','Action'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public salesitemsService: SalesitemsService) { }

  ngOnInit() {
    this.resetForm();
    this.refreshsalesitemsList();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.salesitemsService.selectedSalesitems = {
      _id: "",
      materialdescription: "",
      qty: "",
      used: "",
      instock: "",
    }
  }

  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.salesitemsService.postsalesitems(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshsalesitemsList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    }
    else {
      this.salesitemsService.putsalesitems(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshsalesitemsList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshsalesitemsList() {
    this.salesitemsService.getsalesitemsList().subscribe((res) => {
      this.salesitemsService.salesitemss = res as Salesitems[];
      this.dataSource = new MatTableDataSource(this.salesitemsService.salesitemss);
    });
  }

  onEdit(salesitems: Salesitems) {
    this.salesitemsService.selectedSalesitems = salesitems;
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.salesitemsService.deletesalesitems(_id).subscribe((res) => {
        this.refreshsalesitemsList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
