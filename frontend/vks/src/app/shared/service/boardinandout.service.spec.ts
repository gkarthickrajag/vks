import { TestBed } from '@angular/core/testing';

import { BoardinandoutService } from './boardinandout.service';

describe('BoardinandoutService', () => {
  let service: BoardinandoutService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BoardinandoutService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
