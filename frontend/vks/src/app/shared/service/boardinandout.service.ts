import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Boardinandout} from '../model/boardinandoutModel'

@Injectable()
export class BoardinandoutService {
  selectedBoardinandout: Boardinandout;
  boardinandouts: Boardinandout[];
  readonly baseURL = 'http://localhost:3000/boardinandout';

  constructor(public http: HttpClient) { }

  postboardinandout(boardinandout: Boardinandout) {
    console.log(boardinandout)
    return this.http.post(this.baseURL, boardinandout);
  }

  getboardinandoutList() {
    return this.http.get(this.baseURL);
  }

  putboardinandout(boardinandout: Boardinandout) {
    return this.http.patch(this.baseURL + `/${boardinandout._id}`, boardinandout);
  }

  deleteboardinandout(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

}
