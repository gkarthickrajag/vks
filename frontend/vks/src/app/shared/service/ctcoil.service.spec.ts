import { TestBed } from '@angular/core/testing';

import { CtcoilService } from './ctcoil.service';

describe('CtcoilService', () => {
  let service: CtcoilService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CtcoilService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
