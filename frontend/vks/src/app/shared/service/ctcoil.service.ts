import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Ctcoil} from '../model/ctcoilModel'

@Injectable()
export class CtcoilService {
  selectedCtcoil: Ctcoil;
  ctcoils: Ctcoil[];
  readonly baseURL = 'http://localhost:3000/ctcoil';

  constructor(public http: HttpClient) { }

  postctcoil(ctcoil: Ctcoil) {
    return this.http.post(this.baseURL, ctcoil);
  }

  getctcoilList() {
    return this.http.get(this.baseURL);
  }

  putctcoil(ctcoil: Ctcoil) {
    return this.http.patch(this.baseURL + `/${ctcoil._id}`, ctcoil);
  }

  deletectcoil(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

}
