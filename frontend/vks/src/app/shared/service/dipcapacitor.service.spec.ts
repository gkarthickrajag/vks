import { TestBed } from '@angular/core/testing';

import { DipcapacitorService } from './dipcapacitor.service';

describe('DipcapacitorService', () => {
  let service: DipcapacitorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DipcapacitorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
