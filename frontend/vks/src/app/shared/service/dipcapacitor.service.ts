import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Dipcapacitor} from '../model/dipcapacitorModel'

@Injectable()
export class DipcapacitorService {
  selectedDipcapacitor: Dipcapacitor;
  dipcapacitors: Dipcapacitor[];
  readonly baseURL = 'http://localhost:3000/dipcapacitor';

  constructor(public http: HttpClient) { }

  postdipcapacitor(dipcapacitor: Dipcapacitor) {
    return this.http.post(this.baseURL, dipcapacitor);
  }

  getdipcapacitorList() {
    return this.http.get(this.baseURL);
  }

  putdipcapacitor(dipcapacitor: Dipcapacitor) {
    return this.http.patch(this.baseURL + `/${dipcapacitor._id}`, dipcapacitor);
  }

  deletedipcapacitor(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

}
