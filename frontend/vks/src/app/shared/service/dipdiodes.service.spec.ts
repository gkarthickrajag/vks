import { TestBed } from '@angular/core/testing';

import { DipdiodesService } from './dipdiodes.service';

describe('DipdiodesService', () => {
  let service: DipdiodesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DipdiodesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
