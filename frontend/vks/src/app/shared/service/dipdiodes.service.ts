import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Dipdiodes} from '../model/dipdiodesModel'

@Injectable()
export class DipdiodesService {
  selectedDipdiodes: Dipdiodes;
  dipdiodess: Dipdiodes[];
  readonly baseURL = 'http://localhost:3000/dipdiodes';

  constructor(public http: HttpClient) { }

  postdipdiodes(dipdiodes: Dipdiodes) {
    return this.http.post(this.baseURL, dipdiodes);
  }

  getdipdiodesList() {
    return this.http.get(this.baseURL);
  }

  putdipdiodes(dipdiodes: Dipdiodes) {
    return this.http.patch(this.baseURL + `/${dipdiodes._id}`, dipdiodes);
  }

  deletedipdiodes(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

}
