import { TestBed } from '@angular/core/testing';

import { DipicService } from './dipic.service';

describe('DipicService', () => {
  let service: DipicService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DipicService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
