import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Dipic} from '../model/dipicModel'

@Injectable()
export class DipicService {
  selectedDipic: Dipic;
  dipics: Dipic[];
  readonly baseURL = 'http://localhost:3000/dipic';

  constructor(public http: HttpClient) { }

  postDipic(dipic: Dipic) {
    return this.http.post(this.baseURL, dipic);
  }

  getDipicList() {
    return this.http.get(this.baseURL);
  }

  putDipic(dipic: Dipic) {
    return this.http.patch(this.baseURL + `/${dipic._id}`, dipic);
  }

  deleteDipic(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

}
