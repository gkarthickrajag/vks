import { TestBed } from '@angular/core/testing';

import { DipresistanceService } from './dipresistance.service';

describe('DipresistanceService', () => {
  let service: DipresistanceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DipresistanceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
