import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Dipresistance} from '../model/dipresistanceModel'

@Injectable()
export class DipresistanceService {
  selectedDipresistance: Dipresistance;
  dipresistances: Dipresistance[];
  readonly baseURL = 'http://localhost:3000/dipresistance';

  constructor(public http: HttpClient) { }

  postdipresistance(dipresistance: Dipresistance) {
    return this.http.post(this.baseURL, dipresistance);
  }

  getdipresistanceList() {
    return this.http.get(this.baseURL);
  }

  putdipresistance(dipresistance: Dipresistance) {
    return this.http.patch(this.baseURL + `/${dipresistance._id}`, dipresistance);
  }

  deletedipresistance(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

}
