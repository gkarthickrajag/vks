import { TestBed } from '@angular/core/testing';

import { DiptransistorService } from './diptransistor.service';

describe('DiptransistorService', () => {
  let service: DiptransistorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiptransistorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
