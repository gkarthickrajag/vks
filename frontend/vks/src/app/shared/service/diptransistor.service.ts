import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Diptransistor} from '../model/diptransistorModel'

@Injectable()
export class DiptransistorService {
  selectedDiptransistor: Diptransistor;
  diptransistors: Diptransistor[];
  readonly baseURL = 'http://localhost:3000/diptransistor';

  constructor(public http: HttpClient) { }

  postdiptransistor(diptransistor: Diptransistor) {
    return this.http.post(this.baseURL, diptransistor);
  }

  getdiptransistorList() {
    return this.http.get(this.baseURL);
  }

  putdiptransistor(diptransistor: Diptransistor) {
    return this.http.patch(this.baseURL + `/${diptransistor._id}`, diptransistor);
  }

  deletediptransistor(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

}
