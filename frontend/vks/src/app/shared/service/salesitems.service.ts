import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Salesitems} from '../model/salesitemsModel'

@Injectable()
export class SalesitemsService {
  selectedSalesitems: Salesitems;
  salesitemss: Salesitems[];
  readonly baseURL = 'http://localhost:3000/salesitems';

  constructor(public http: HttpClient) { }

  postsalesitems(salesitems: Salesitems) {
    return this.http.post(this.baseURL, salesitems);
  }

  getsalesitemsList() {
    return this.http.get(this.baseURL);
  }

  putsalesitems(salesitems: Salesitems) {
    return this.http.patch(this.baseURL + `/${salesitems._id}`, salesitems);
  }

  deletesalesitems(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

}
