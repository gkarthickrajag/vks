import { TestBed } from '@angular/core/testing';

import { SmdcapacitorService } from './smdcapacitor.service';

describe('SmdcapacitorService', () => {
  let service: SmdcapacitorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SmdcapacitorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
