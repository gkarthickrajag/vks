import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Smdcapacitor} from '../model/smdcapacitorModel'

@Injectable()
export class SmdcapacitorService {
  selectedSmdcapacitor: Smdcapacitor;
  smdcapacitors: Smdcapacitor[];
  readonly baseURL = 'http://localhost:3000/smdcapacitor';

  constructor(public http: HttpClient) { }

  postsmdcapacitor(smdcapacitor: Smdcapacitor) {
    return this.http.post(this.baseURL, smdcapacitor);
  }

  getsmdcapacitorList() {
    return this.http.get(this.baseURL);
  }

  putsmdcapacitor(smdcapacitor: Smdcapacitor) {
    return this.http.patch(this.baseURL + `/${smdcapacitor._id}`, smdcapacitor);
  }

  deletesmdcapacitor(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

}
