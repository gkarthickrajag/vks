import { TestBed } from '@angular/core/testing';

import { SmddiodesService } from './smddiodes.service';

describe('SmddiodesService', () => {
  let service: SmddiodesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SmddiodesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
