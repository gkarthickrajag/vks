import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Smddiodes} from '../model/smddiodesModel'

@Injectable()
export class SmddiodesService {
  selectedSmddiodes: Smddiodes;
  smddiodess: Smddiodes[];
  readonly baseURL = 'http://localhost:3000/smddiodes';

  constructor(public http: HttpClient) { }

  postsmddiodes(smddiodes: Smddiodes) {
    return this.http.post(this.baseURL, smddiodes);
  }

  getsmddiodesList() {
    return this.http.get(this.baseURL);
  }

  putsmddiodes(smddiodes: Smddiodes) {
    return this.http.patch(this.baseURL + `/${smddiodes._id}`, smddiodes);
  }

  deletesmddiodes(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

}
