import { TestBed } from '@angular/core/testing';

import { SmdicService } from './smdic.service';

describe('SmdicService', () => {
  let service: SmdicService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SmdicService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
