import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Smdic} from '../model/smdicModel'

@Injectable()
export class SmdicService {
  selectedSmdic: Smdic;
  smdics: Smdic[];
  readonly baseURL = 'http://localhost:3000/smdic';

  constructor(public http: HttpClient) { }

  postsmdic(smdic: Smdic) {
    return this.http.post(this.baseURL, smdic);
  }

  getsmdicList() {
    return this.http.get(this.baseURL);
  }

  putsmdic(smdic: Smdic) {
    return this.http.patch(this.baseURL + `/${smdic._id}`, smdic);
  }

  deletesmdic(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

}
