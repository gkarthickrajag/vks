import { TestBed } from '@angular/core/testing';

import { SmdresistanceService } from './smdresistance.service';

describe('SmdresistanceService', () => {
  let service: SmdresistanceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SmdresistanceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
