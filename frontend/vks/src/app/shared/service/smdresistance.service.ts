import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Smdresistance} from '../model/smdresistanceModel'

@Injectable()
export class SmdresistanceService {
  selectedSmdresistance: Smdresistance;
  smdresistances: Smdresistance[];
  readonly baseURL = 'http://localhost:3000/smdresistance';

  constructor(public http: HttpClient) { }

  postsmdresistance(smdresistance: Smdresistance) {
    return this.http.post(this.baseURL, smdresistance);
  }

  getsmdresistanceList() {
    return this.http.get(this.baseURL);
  }

  putsmdresistance(smdresistance: Smdresistance) {
    return this.http.patch(this.baseURL + `/${smdresistance._id}`, smdresistance);
  }

  deletesmdresistance(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

}
