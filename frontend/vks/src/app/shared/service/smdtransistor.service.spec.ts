import { TestBed } from '@angular/core/testing';

import { SmdtransistorService } from './smdtransistor.service';

describe('SmdtransistorService', () => {
  let service: SmdtransistorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SmdtransistorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
