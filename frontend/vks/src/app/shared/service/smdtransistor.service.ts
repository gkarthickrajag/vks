import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Smdtransistor} from '../model/smdtransistorModel'

@Injectable()
export class SmdtransistorService {
  selectedSmdtransistor: Smdtransistor;
  smdtransistors: Smdtransistor[];
  readonly baseURL = 'http://localhost:3000/smdtransistor';

  constructor(public http: HttpClient) { }

  postsmdtransistor(smdtransistor: Smdtransistor) {
    return this.http.post(this.baseURL, smdtransistor);
  }

  getsmdtransistorList() {
    return this.http.get(this.baseURL);
  }

  putsmdtransistor(smdtransistor: Smdtransistor) {
    return this.http.patch(this.baseURL + `/${smdtransistor._id}`, smdtransistor);
  }

  deletesmdtransistor(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

}
