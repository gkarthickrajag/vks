import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmdcapacitorComponent } from './smdcapacitor.component';

describe('SmdcapacitorComponent', () => {
  let component: SmdcapacitorComponent;
  let fixture: ComponentFixture<SmdcapacitorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmdcapacitorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmdcapacitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
