import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {SmdcapacitorService} from '../shared/service/smdcapacitor.service';
import {Smdcapacitor} from '../shared/model/smdcapacitorModel'
import { MatTableDataSource } from '@angular/material/table';


declare var M: any;

@Component({
  selector: 'app-smdcapacitor',
  templateUrl: './smdcapacitor.component.html',
  styleUrls: ['./smdcapacitor.component.scss'],
  providers: [SmdcapacitorService]
})
export class SmdcapacitorComponent implements OnInit {
  displayedColumns: string[] = ['values', 'qty', 'Used', 'InStock','Action'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public smdcapacitorService: SmdcapacitorService) { }

  ngOnInit() {
    this.resetForm();
    this.refreshsmdcapacitorList();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.smdcapacitorService.selectedSmdcapacitor = {
      _id: "",
      values: "",
      qty: "",
      used: "",
      instock: "",
    }
  }

  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.smdcapacitorService.postsmdcapacitor(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshsmdcapacitorList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    }
    else {
      this.smdcapacitorService.putsmdcapacitor(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshsmdcapacitorList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshsmdcapacitorList() {
    this.smdcapacitorService.getsmdcapacitorList().subscribe((res) => {
      this.smdcapacitorService.smdcapacitors = res as Smdcapacitor[];
      this.dataSource = new MatTableDataSource(this.smdcapacitorService.smdcapacitors);
    });
  }

  onEdit(smdcapacitor: Smdcapacitor) {
    this.smdcapacitorService.selectedSmdcapacitor = smdcapacitor;
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.smdcapacitorService.deletesmdcapacitor(_id).subscribe((res) => {
        this.refreshsmdcapacitorList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
