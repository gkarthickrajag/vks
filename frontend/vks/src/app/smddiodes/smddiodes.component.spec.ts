import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmddiodesComponent } from './smddiodes.component';

describe('SmddiodesComponent', () => {
  let component: SmddiodesComponent;
  let fixture: ComponentFixture<SmddiodesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmddiodesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmddiodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
