import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {SmddiodesService} from '../shared/service/smddiodes.service';
import {Smddiodes} from '../shared/model/smddiodesModel'
import { MatTableDataSource } from '@angular/material/table';


declare var M: any;

@Component({
  selector: 'app-smddiodes',
  templateUrl: './smddiodes.component.html',
  styleUrls: ['./smddiodes.component.scss'],
  providers: [SmddiodesService]
})
export class SmddiodesComponent implements OnInit {
  displayedColumns: string[] = ['Marking','Values', 'qty', 'Used', 'InStock','Action'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  constructor(public smddiodesService: SmddiodesService) { }

  ngOnInit() {
    this.resetForm();
    this.refreshsmddiodesList();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.smddiodesService.selectedSmddiodes = {
      _id: "",
      marking:"",
      values: "",
      qty: "",
      used: "",
      instock: "",
    }
  }

  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.smddiodesService.postsmddiodes(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshsmddiodesList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    }
    else {
      this.smddiodesService.putsmddiodes(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshsmddiodesList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshsmddiodesList() {
    this.smddiodesService.getsmddiodesList().subscribe((res) => {
      this.smddiodesService.smddiodess = res as Smddiodes[];
      this.dataSource = new MatTableDataSource(this.smddiodesService.smddiodess);
    });
    
  }

  onEdit(smddiodes: Smddiodes) {
    this.smddiodesService.selectedSmddiodes = smddiodes;
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.smddiodesService.deletesmddiodes(_id).subscribe((res) => {
        this.refreshsmddiodesList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
