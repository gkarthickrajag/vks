import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmdicComponent } from './smdic.component';

describe('SmdicComponent', () => {
  let component: SmdicComponent;
  let fixture: ComponentFixture<SmdicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmdicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmdicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
