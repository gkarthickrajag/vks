import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {SmdicService} from '../shared/service/smdic.service';
import {Smdic} from '../shared/model/smdicModel'
import { MatTableDataSource } from '@angular/material/table';


declare var M: any;

@Component({
  selector: 'app-smdic',
  templateUrl: './smdic.component.html',
  styleUrls: ['./smdic.component.scss'],
  providers: [SmdicService]
})
export class SmdicComponent implements OnInit {
  displayedColumns: string[] = ['IcNumber', 'qty', 'Used', 'InStock','Action'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public smdicService: SmdicService) { }

  ngOnInit() {
    this.resetForm();
    this.refreshsmdicList();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.smdicService.selectedSmdic = {
      _id: "",
      icnumber: "",
      qty: "",
      used: "",
      instock: "",
    }
  }

  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.smdicService.postsmdic(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshsmdicList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    }
    else {
      this.smdicService.putsmdic(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshsmdicList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshsmdicList() {
    this.smdicService.getsmdicList().subscribe((res) => {
      this.smdicService.smdics = res as Smdic[];
      this.dataSource = new MatTableDataSource(this.smdicService.smdics);
    });
  }

  onEdit(smdic: Smdic) {
    this.smdicService.selectedSmdic = smdic;
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.smdicService.deletesmdic(_id).subscribe((res) => {
        this.refreshsmdicList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
