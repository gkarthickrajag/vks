import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmdresistanceComponent } from './smdresistance.component';

describe('SmdresistanceComponent', () => {
  let component: SmdresistanceComponent;
  let fixture: ComponentFixture<SmdresistanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmdresistanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmdresistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
