import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {SmdresistanceService} from '../shared/service/smdresistance.service';
import {Smdresistance} from '../shared/model/smdresistanceModel'
import { MatTableDataSource } from '@angular/material/table';


declare var M: any;

@Component({
  selector: 'app-smdresistance',
  templateUrl: './smdresistance.component.html',
  styleUrls: ['./smdresistance.component.scss'],
  providers: [SmdresistanceService]
})
export class SmdresistanceComponent implements OnInit {
  displayedColumns: string[] = ['Range', 'qty', 'Used', 'InStock','Action'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public smdresistanceService: SmdresistanceService) { }

  ngOnInit() {
    this.resetForm();
    this.refreshsmdresistanceList();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.smdresistanceService.selectedSmdresistance = {
      _id: "",
      range: "",
      qty: "",
      used: "",
      instock: "",
    }
  }

  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.smdresistanceService.postsmdresistance(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshsmdresistanceList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    }
    else {
      this.smdresistanceService.putsmdresistance(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshsmdresistanceList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshsmdresistanceList() {
    this.smdresistanceService.getsmdresistanceList().subscribe((res) => {
      this.smdresistanceService.smdresistances = res as Smdresistance[];
      this.dataSource = new MatTableDataSource(this.smdresistanceService.smdresistances);
    });
  }

  onEdit(smdresistance: Smdresistance) {
    this.smdresistanceService.selectedSmdresistance = smdresistance;
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.smdresistanceService.deletesmdresistance(_id).subscribe((res) => {
        this.refreshsmdresistanceList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
