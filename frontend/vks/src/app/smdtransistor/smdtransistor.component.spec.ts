import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmdtransistorComponent } from './smdtransistor.component';

describe('SmdtransistorComponent', () => {
  let component: SmdtransistorComponent;
  let fixture: ComponentFixture<SmdtransistorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmdtransistorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmdtransistorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
