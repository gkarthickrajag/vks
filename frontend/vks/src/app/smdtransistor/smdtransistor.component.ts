import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {SmdtransistorService} from '../shared/service/smdtransistor.service';
import {Smdtransistor} from '../shared/model/smdtransistorModel'
import { MatTableDataSource } from '@angular/material/table';


declare var M: any;

@Component({
  selector: 'app-smdtransistor',
  templateUrl: './smdtransistor.component.html',
  styleUrls: ['./smdtransistor.component.scss'],
  providers: [SmdtransistorService]
})
export class SmdtransistorComponent implements OnInit {
  displayedColumns: string[] = ['Marking','Values','qty', 'Used', 'InStock','Action'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public smdtransistorService: SmdtransistorService) { }

  ngOnInit() {
    this.resetForm();
    this.refreshsmdtransistorList();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.smdtransistorService.selectedSmdtransistor = {
      _id: "",
      marking: "",
      values:"",
      qty: "",
      used: "",
      instock: "",
    }
  }

  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.smdtransistorService.postsmdtransistor(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshsmdtransistorList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    }
    else {
      this.smdtransistorService.putsmdtransistor(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshsmdtransistorList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshsmdtransistorList() {
    this.smdtransistorService.getsmdtransistorList().subscribe((res) => {
      this.smdtransistorService.smdtransistors = res as Smdtransistor[];
      this.dataSource = new MatTableDataSource(this.smdtransistorService.smdtransistors);
    });
  }

  onEdit(smdtransistor: Smdtransistor) {
    this.smdtransistorService.selectedSmdtransistor = smdtransistor;
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.smdtransistorService.deletesmdtransistor(_id).subscribe((res) => {
        this.refreshsmdtransistorList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
